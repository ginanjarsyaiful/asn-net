﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Asn1DecoderNet5;
using Asn1DecoderNet5.Interfaces;
using Huawei;
using PeNet.Asn1;
using Asn1;
using Oss.Asn1;

namespace ConsoleApplication
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            using (Stream SourceStream = File.OpenRead(@"C:\JK30_DIH_200401205614"))
            {

                //Metode 1
                byte[] buffer = ReadFully(SourceStream);
                //ITag decodedDataStructure = Asn1DecoderNet5.Decoder.Decode(buffer);
                SourceStream.Close();

                //Metode 2
                //DerCodec der = new Huawei.DerCodec();
                //BasePdu pdu = new BasePdu();

                //der.(SourceStream, Basepd);

            }
        }
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
